/**
 *
 * Copyright (c) 2016 - 2018, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#include <bonjour/common.h>

#include <memory>

#include <sys/select.h>
#include <sys/sysctl.h>

namespace gp_bonjour
{

Common::~Common()
{
}

DNSServiceErrorType Common::wait_for_socket_read_ready(int sockfd, std::chrono::milliseconds timeout)
{
    int nfds = sockfd + 1;

    fd_set readfds;
    FD_SET(sockfd, &readfds);

    struct timespec t;
    t.tv_sec  =  timeout.count() / 1000;
    t.tv_nsec = (timeout.count() % 1000) * 1000000;

    const auto result = pselect(nfds, &readfds, nullptr, nullptr, (timeout.count() > 0 ? &t : nullptr), nullptr);
    if (result > 0)
    {
        err_ = DNSServiceProcessResult(dnssref_);
    }
    else if (result == 0)
    {
        err_ = kDNSServiceErr_Timeout;
    }
    else // (result == -1)
    {
        err_ = kDNSServiceErr_Invalid;
    }

    return err_;
}

std::future<DNSServiceErrorType> Common::make_result() const
{
    std::promise<DNSServiceErrorType> promise;
    promise.set_value(err_);
    return promise.get_future();
}

// Utilities

// Based on https://stackoverflow.com/a/25930208/2233452
std::string system_info_string(const char* attribute_name)
{
    size_t size;
    sysctlbyname(attribute_name, nullptr, &size, nullptr, 0); // Get the size of the data.
    std::string attribute_value(size, 0);
    int err = sysctlbyname(attribute_name, &attribute_value[0], &size, nullptr, 0);
    if (err != 0)
    {
        return "";
    }
    return attribute_value;
}

std::string get_hostname_apple()
{
    constexpr int BUFSIZE = 128;
    char buffer[BUFSIZE];

    std::string result;
    std::shared_ptr<FILE> pipe(popen("scutil --get LocalHostName", "r"), pclose);
    if (!pipe)
    {
        return "";
    }

    while (!feof(pipe.get()))
    {
        if (fgets(buffer, BUFSIZE, pipe.get()) != nullptr)
        {
            result += buffer;
        }
    }

    result.erase(std::remove_if(result.begin(), result.end(), isspace), result.end());
    return result;
}

std::string get_hostname()
{
#ifdef _WIN32
    return "UNIMPLEMENTED"
#elif __APPLE__
    return get_hostname_apple()
#elif __linux__
    return system_info_string("kern.hostname")
#elif __unix__ // all unices not caught above
    return "UNIMPLEMENTED"
#elif defined(_POSIX_VERSION)
    return "UNIMPLEMENTED"
#else
#  error "Unknown compiler"
#endif
        + ".local.";
}

}
