/**
 *
 * Copyright (c) 2016 - 2018, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#include <bonjour/registrar.h>

#include <sys/select.h>

namespace gp_bonjour
{

Registrar::~Registrar()
{
    unregister_service();
}

std::future<DNSServiceErrorType> Registrar::register_service(const Record& record, std::uint16_t port)
{
    if (dnssref_)
    {
        err_ = kDNSServiceErr_AlreadyRegistered;
        return make_result();
    }

    err_ = DNSServiceRegister(&dnssref_, 0, 0,
                              record.service_name_.empty() ? nullptr : record.service_name_.c_str(),
                              record.type_.c_str(),
                              record.domain_.empty() ? nullptr : record.domain_.c_str(),
                              nullptr, htons(port),
                              0, nullptr,
                              &Registrar::registration_callback, this);

    if (err_ == kDNSServiceErr_NoError)
    {
        int sockfd = DNSServiceRefSockFD(dnssref_);
        if (sockfd == -1)
        {
            err_ = kDNSServiceErr_Invalid;
            return make_result();
        }
        else
        {
            return std::async(std::launch::async, [this, sockfd]() { return wait_for_socket_read_ready(sockfd); });
        }
    }

    return make_result();
}

void Registrar::unregister_service()
{
    if (dnssref_)
    {
        DNSServiceRefDeallocate(dnssref_);
        dnssref_ = nullptr;
    }
}

void Registrar::registration_callback(DNSServiceRef, DNSServiceFlags, DNSServiceErrorType error_code,
                                      const char *name, const char* type, const char* domain, void* data)
{
    Registrar* registrar = static_cast<Registrar*>(data);
    registrar->err_ = error_code;

    if (registrar->err_ == kDNSServiceErr_NoError)
    {
        registrar->registered_record_ = Record(name, type, domain);
    }
}

}
