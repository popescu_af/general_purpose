/**
 *
 * Copyright (c) 2016 - 2018, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#include <bonjour/browser.h>

#include <algorithm>

#include <sys/select.h>

using namespace std::chrono_literals;

namespace gp_bonjour
{

Browser::~Browser()
{
}

DNSServiceErrorType Browser::browse_services_of_type(const std::string& type)
{
    err_ = DNSServiceBrowse(&dnssref_, 0, 0, type.c_str(), nullptr, &Browser::browse_callback, this);

    if (err_ == kDNSServiceErr_NoError)
    {
        int sockfd = DNSServiceRefSockFD(dnssref_);
        if (sockfd == -1)
        {
            err_ = kDNSServiceErr_Invalid;
            return err_;
        }
        else
        {
            auto browsing_procedure =
                [this, sockfd]()
                {
                    std::unique_lock<std::mutex> lock(cv_mutex_);
                    while (end_browsing_.wait_for(lock, 50ms) == std::cv_status::timeout
                           && wait_for_socket_read_ready(sockfd, 50ms) != kDNSServiceErr_Invalid);

                    DNSServiceRefDeallocate(dnssref_);
                    dnssref_ = nullptr;
                };
            browsing_ended_ = std::async(std::launch::async, browsing_procedure);
        }
    }

    return err_;
}

void Browser::stop_browsing()
{
    if (dnssref_)
    {
        {
            std::unique_lock<std::mutex> lock(cv_mutex_);
            end_browsing_.notify_one();
        }
        browsing_ended_.wait();
    }
}

void Browser::browse_callback(DNSServiceRef, DNSServiceFlags flags, std::uint32_t, DNSServiceErrorType error_code,
                              const char* name, const char* type, const char* domain, void* context)
{
    Browser* browser = static_cast<Browser*>(context);
    browser->err_ = error_code;

    if (browser->err_ == kDNSServiceErr_NoError)
    {
        Record record(name, type, domain);
        std::unique_lock<std::mutex> lock(browser->data_mutex_);

        // Perform add / remove
        auto& records = browser->records_;
        if (flags & kDNSServiceFlagsAdd)
        {
            auto pos = std::lower_bound(records.begin(), records.end(), record);
            if (pos == records.end() || *pos != record)
            {
                records.emplace(pos, std::move(record));
            }
        }
        else
        {
            records.erase(std::remove(records.begin(), records.end(), record), records.end());
        }

        if (!(flags & kDNSServiceFlagsMoreComing))
        {
            auto& notification_count = browser->notification_count_;
            auto& pending_notification = browser->pending_notification_;
            if (notification_count && !pending_notification.valid())
            {
                pending_notification.wait(); // Wait for previous notification to finish.
            }
            ++notification_count;
            pending_notification = std::async(std::launch::async,
                                              [browser]() { if (browser->cb_) { browser->cb_(); } });
        }
    }
}

}
