/**
 *
 * Copyright (c) 2016 - 2018, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#include "logger/logger.h"

#ifndef WIN32
#include <unistd.h>
#endif

#include <cstdarg>
#include <cstdio>
#include <mutex>

#define GP_LOG_NO_COLOR

namespace gp
{
namespace log
{
static inline const bool& use_color()
{
#if defined(GP_LOG_NO_COLOR) || defined(WIN32)
    static const bool use = 0;
#else
    static const bool use = isatty(1);
#endif
    return use;
}

static inline void msg(const char* format, const int color, va_list args)
{
    if (use_color())
    {
        fprintf(stdout, "\x1b[0;%dm", color);
    }

    vfprintf(stdout, format, args);

    if (use_color())
    {
        fprintf(stdout, "\x1b[0m\n");
    }
    else
    {
        fprintf(stdout, "\n");
    }
}

std::string print2str(const char* format, ...)
{
    static char buffer[512];

    va_list args;
    va_start(args, format);
    vsprintf(buffer, format, args);
    va_end(args);

    return std::string(buffer);
}

void print(const Level level, const int color, const char* format, ...)
{
    static std::mutex mutex;
    std::unique_lock<std::mutex> lock(mutex);

    if (level <= get_level())
    {
        va_list args;
        va_start(args, format);
        msg(format, color, args);
        va_end(args);
    }
}

static Level& log_level()
{
    static Level level = Level::LVL_ERROR;
    return level;
}

void set_level(const Level level) { log_level() = level; }

Level get_level() { return log_level(); }
}
}
