/**
 *
 * Copyright (c) 2016, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#include "gp/connection.h"
#include "gp/logger.h"
#include "gp/gp.h"

#include <array>
#include <atomic>
#include <cassert>
#include <condition_variable>
#include <thread>

#include <boost/bind.hpp>
#include <boost/thread.hpp>

namespace gp
{
namespace inet
{
namespace asio = boost::asio;
namespace ip = boost::asio::ip;

using namespace ip;

//-- TCP Endpoint ------------------------------------------------------------------------------------------------------
struct TcpEndpoint::TcpEndpointImpl
{
    TcpEndpointImpl()
        : io_service_()
        , work_(io_service_)
        , thr_grp_()
        , stop_(false)
        , stop_var_()
        , mutex_()
        , socket_(io_service_)
        , send_queue_()
        , recv_chunk_()
        , recv_buf_()
        , read_index_(0)
        , write_index_(0)
    {
    }

    ~TcpEndpointImpl() { stop(); }

    void start() { thr_grp_.create_thread(boost::bind(&asio::io_service::run, &io_service_)); }

    void stop()
    {
        std::unique_lock<std::mutex> lock(mutex_);
        stop_ = true;
        if (!send_queue_.empty())
        {
            stop_var_.wait(lock);
        }
        io_service_.stop();
        thr_grp_.join_all();
    }

    void queued_send(const std::string& buffer)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        if (stop_)
        {
            return;
        }

        const bool stopped = io_service_.stopped();
        GP_UNUSED(stopped);

        send_queue_.push_back(buffer);

        if (send_queue_.size() == 1)
        {
            asio::async_write(socket_, asio::buffer(send_queue_.front()),
                              boost::bind(&TcpEndpointImpl::queued_send_ack_fun, this, asio::placeholders::error,
                                          asio::placeholders::bytes_transferred));
        }
    }

    void queued_send_ack_fun(const ErrorCode& error_code, size_t bytes_transferred)
    {
        std::unique_lock<std::mutex> lock(mutex_);
        if (stop_)
        {
            stop_var_.notify_all();
            return;
        }

        GP_UNUSED(error_code);
        GP_UNUSED(bytes_transferred);
        GP_LOG_INFO("Server - Error code: %d, Bytes transferred: %lld", error_code.value(), bytes_transferred);

        send_queue_.pop_front();

        // Send next
        if (!send_queue_.empty())
        {
            asio::async_write(socket_, asio::buffer(send_queue_.front()),
                              boost::bind(&TcpEndpointImpl::queued_send_ack_fun, this, asio::placeholders::error,
                                          asio::placeholders::bytes_transferred));
        }
    }

    size_t bytes_available() const
    {
        if (write_index_ >= read_index_)
        {
            return write_index_ - read_index_;
        }

        return RECV_BUF_SIZE - read_index_ + write_index_;
    }

    static const size_t BAD_INDEX = 0xffffffff;

    size_t get_next_write_index(const size_t len, ErrorCode& error_code) const
    {
        if (len > RECV_BUF_SIZE)
        {
            GP_SET_ERROR(error_code, no_buffer_space);
            return BAD_INDEX;
        }

        const size_t new_write_index_ovf = write_index_ + len;
        const size_t new_write_index = new_write_index_ovf % RECV_BUF_SIZE;

        if (new_write_index_ovf != new_write_index && new_write_index > read_index_)
        {
            GP_SET_ERROR(error_code, no_buffer_space);
            return BAD_INDEX;
        }

        return new_write_index;
    }

    void recv(std::string& result, ErrorCode& error_code, size_t bytes_expected = 0)
    {
        if (bytes_expected == 0)
        {
            bytes_expected = bytes_available();
        }

        while (bytes_available() == 0 || bytes_expected > bytes_available())
        {
            const size_t len = socket_.read_some(asio::buffer(recv_chunk_), error_code);
            if (error_code && error_code != asio::error::eof)
            {
                GP_FAIL_IF_ERROR(error_code);
            }

            const size_t new_write_index = get_next_write_index(len, error_code);
            GP_FAIL_IF_ERROR(error_code);

            // Copy data into recv buffer
            const size_t size_to_fit_at_end = RECV_BUF_SIZE - write_index_;
            const size_t to_copy_at_end = std::min(size_to_fit_at_end, len);
            memcpy(&recv_buf_[write_index_], recv_chunk_.data(), to_copy_at_end);

            if (to_copy_at_end != len)
            {
                memcpy(&recv_buf_[0], recv_chunk_.data() + to_copy_at_end, len - to_copy_at_end);
            }

            // Update write index
            write_index_ = new_write_index;
        }

        // Update bytes_expected if needed
        if (bytes_expected == 0)
        {
            bytes_expected = bytes_available();
        }

        result.resize(bytes_expected, ' ');

        // Copy from read index in two steps, in case of read index overflow
        const size_t bytes_left_until_buffer_end = RECV_BUF_SIZE - read_index_;
        const size_t to_copy_from_end =
            (bytes_left_until_buffer_end < bytes_expected) ? bytes_left_until_buffer_end : bytes_expected;

        memcpy(&result[0], recv_buf_.data() + read_index_, to_copy_from_end);

        // Update read index
        read_index_ = (read_index_ + to_copy_from_end) % RECV_BUF_SIZE;

        if (to_copy_from_end != bytes_expected)
        {
            const size_t to_copy = bytes_expected - to_copy_from_end;
            memcpy(&result[to_copy_from_end], recv_buf_.data() + read_index_, to_copy);

            // Update read index
            read_index_ = (read_index_ + to_copy) % RECV_BUF_SIZE;
        }
    }

    // IO
    asio::io_service io_service_;
    asio::io_service::work work_;
    boost::thread_group thr_grp_;

    // Stopping
    bool stop_;
    std::condition_variable stop_var_;
    std::mutex mutex_;

    // TCP
    asio::ip::tcp::socket socket_;
    std::deque<std::string> send_queue_;

    // Recv data
    static const size_t MTU_SIZE = 1536;
    static const size_t RECV_BUF_SIZE = 16384;
    std::array<char, MTU_SIZE> recv_chunk_;
    std::array<char, RECV_BUF_SIZE> recv_buf_;
    size_t read_index_;
    size_t write_index_;
};

TcpEndpoint::TcpEndpoint() : impl_(new TcpEndpointImpl()) {}

TcpEndpoint::~TcpEndpoint() { stop(); }

void TcpEndpoint::start() { impl_->start(); }

void TcpEndpoint::stop() { impl_->stop(); }

void TcpEndpoint::queued_send(const std::string& buffer) { impl_->queued_send(buffer); }

void TcpEndpoint::recv(std::string& result, ErrorCode& error_code, size_t bytes_expected)
{
    impl_->recv(result, error_code, bytes_expected);
}

//-- TCP Client --------------------------------------------------------------------------------------------------------
TcpClient::TcpClient() : endpoint_() {}

TcpClient::~TcpClient() { disconnect(); }

void TcpClient::connect(const std::string& host, const Port port, ErrorCode& error_code)
{
    // Resolve host
    tcp::resolver resolver(endpoint_.impl_->io_service_);
    tcp::resolver::query query(host, std::to_string(port));
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query, error_code);
    GP_FAIL_IF_ERROR(error_code);

    // Connect
    asio::connect(endpoint_.impl_->socket_, endpoint_iterator, error_code);
    GP_FAIL_IF_ERROR(error_code);

    // Start the IO service
    endpoint_.start();
}

void TcpClient::disconnect() { endpoint_.stop(); }

void TcpClient::queued_send(const std::string& buffer) { endpoint_.queued_send(buffer); }

void TcpClient::recv(std::string& result, ErrorCode& error_code, size_t bytes_expected)
{
    endpoint_.recv(result, error_code, bytes_expected);
}

//-- Abstract Server ---------------------------------------------------------------------------------------------------
AbstractServer::~AbstractServer() { endpoint_->stop(); }

void AbstractServer::accept(TcpEndpoint::pointer endpoint)
{
    endpoint_ = endpoint;

    // Start IO
    endpoint_->start();

    // Start serving
    serve();
}

//-- TCP Server --------------------------------------------------------------------------------------------------------
struct TcpServerImpl
{
    explicit TcpServerImpl(AbstractServer::pointer server)
        : io_service_(), work_(io_service_), thr_grp_(), acceptor_(io_service_), server_(server)
    {
    }

    ~TcpServerImpl() { stop(); }

    void start(const Port port)
    {
        // Prepare accepting
        acceptor_ = tcp::acceptor(io_service_, tcp::endpoint(tcp::v4(), port));

        // Start accepting
        start_accept();

        // Start IO service thread(s)
        thr_grp_.create_thread(boost::bind(&asio::io_service::run, &io_service_));
    }

    void stop()
    {
        io_service_.stop();
        thr_grp_.join_all();
    }

  private:
    void start_accept()
    {
        TcpEndpoint::pointer new_endpoint(new TcpEndpoint);
        acceptor_.async_accept(new_endpoint->impl_->socket_, boost::bind(&TcpServerImpl::handle_accept, this,
                                                                         new_endpoint, asio::placeholders::error));
    }

    void handle_accept(TcpEndpoint::pointer new_endpoint, const ErrorCode& error)
    {
        // Handle established connection
        if (!error)
        {
            server_->accept(new_endpoint);
        }

        // Accept next connection
        start_accept();
    }

    // IO
    asio::io_service io_service_;
    asio::io_service::work work_;
    boost::thread_group thr_grp_;
    tcp::acceptor acceptor_;
    AbstractServer::pointer server_;
};

TcpServer::TcpServer(AbstractServer::pointer server) : impl_(new TcpServerImpl(server)) {}

TcpServer::~TcpServer() { stop(); }

void TcpServer::start(const Port port) { impl_->start(port); }

void TcpServer::stop() { impl_->stop(); }
}
}
