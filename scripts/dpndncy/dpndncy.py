import argparse
import importlib

def main():
    # parse args
    parser = argparse.ArgumentParser(description='Resolve dependency python script: resolve any C++ dependency')
    parser.add_argument('repository', metavar='repository', type=str,
                        help='Path to the directory where the dependency will be resolved.')
    parser.add_argument('depname', metavar='depname', type=str,
                        help='Name of the dependency to resolve.')
    parser.add_argument('--version', dest='swversion', type=str, default='latest',
                        help='Version to build (default=\'latest\')')
    parser.add_argument('--os', dest='ostype', type=str, default='local', help='OS to build for (default=\'local\')')
    parser.add_argument('--arch', dest='archtype', type=str, default='default',
                        help='Architecture to build for (default=\'default\')')
    parser.add_argument('-f', dest='force_rebuild', action='store_const',
                        const=True, default=False,
                        help='Force build (overwrites any previous build for the selected OS/arch)')
    parser.add_argument('--mod', dest='module_list', type=str, default='',
                        help='Comma-separated list of modules to build')
    args = parser.parse_args()

    # Import & instantiate the custom resolver module and run it
    imported = importlib.import_module('resolvers.%s' % args.depname)
    resolver = imported.CustomResolver(args)
    resolver.run()

if __name__ == '__main__':
    main()
