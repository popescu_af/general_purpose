import abstract
import os
import shutil
import subprocess
import utils

#------------------------------------------------#
# Custom resolver for QR-code-generator library. #
#------------------------------------------------#
class CustomResolver(abstract.Resolver):
    def __init__(self, cli_params):
        resolver_params = type('', (), {})()
        resolver_params.version_configurers = {'':self.config_generic}
        resolver_params.latest_version = ''
        resolver_params.supported_os_types = ['local']
        super(CustomResolver, self).__init__(cli_params, resolver_params)

    def name_impl(self):
        return 'qrcodegen'

    def source_impl(self):
        self.git_clone_and_checkout('https://github.com/nayuki/QR-Code-generator.git')

    def config_generic(self):
        pass

    def make_impl(self):
        subprocess.call('g++ -c -std=c++11 %s cpp/BitBuffer.cpp' % self.make_arguments, shell = True)
        subprocess.call('g++ -c -std=c++11 %s cpp/QrCode.cpp'    % self.make_arguments, shell = True)
        subprocess.call('g++ -c -std=c++11 %s cpp/QrSegment.cpp' % self.make_arguments, shell = True)
        subprocess.call('ar rvs libqrcodegenerator.a BitBuffer.o QrCode.o QrSegment.o', shell = True)

    def finish_impl(self):
        utils.create_dir_if_needed(self.incdir)
        utils.create_dir_if_needed(self.libdir)
        shutil.copy(os.path.join('.', 'cpp', 'BitBuffer.hpp'), os.path.join(self.incdir, 'BitBuffer.hpp'))
        shutil.copy(os.path.join('.', 'cpp', 'QrCode.hpp'   ), os.path.join(self.incdir, 'QrCode.hpp'))
        shutil.copy(os.path.join('.', 'cpp', 'QrSegment.hpp'), os.path.join(self.incdir, 'QrSegment.hpp'))
        shutil.move(os.path.join('.', 'libqrcodegenerator.a'), os.path.join(self.libdir, 'libqrcodegenerator.a'))
        os.remove('BitBuffer.o')
        os.remove('QrCode.o')
        os.remove('QrSegment.o')
