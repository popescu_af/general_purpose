import exceptions
import os
import re
import shutil
import subprocess
import sys
import utils

class ResolverError(Exception):
    pass

class Resolver(object):
    # cli_params: command line parameters    {repository, swversion, ostype, archtype}
    # cr_params : custom resolver parameters {version_configurers, latest_version, supported_os_types}
    def __init__(self, cli_params, cr_params):
        # Check to be on Mac for iOS builds
        proc = subprocess.Popen('uname', shell = True, stdout = subprocess.PIPE)
        self.hostos = proc.communicate()[0].strip()
        if self.hostos != 'Darwin' and (cli_params.ostype == 'iOS' or cli_params.ostype == 'iPhoneSimulator'):
            raise ResolverError("Cannot build for iOS on a system that is not a Mac")
        # Set configurers, environmenters
        self.os_configurers = {'local':self.config_local, 'iOS':self.config_ios,
                               'iPhoneSimulator':self.config_iphone_sim}
        self.environmenters = {'local':self.environment_local, 'iOS':self.environment_ios,
                               'iPhoneSimulator':self.environment_iphone_sim}
        # Save current dir
        self.cwd = os.path.abspath(os.getcwd())
        # Check repository is valid & cd repository
        self.base = os.path.abspath(cli_params.repository)
        if not os.path.exists(self.base):
            raise IOError
        os.chdir(self.base)
        # Save parameters
        self.swversion = cli_params.swversion
        self.ostype = cli_params.ostype
        self.arch = cli_params.archtype
        self.modules = cli_params.module_list
        # Save base path for the custom resolver
        base = os.path.join(self.name(), self.swversion)
        utils.create_dir_if_needed(base)
        os.chdir(base)
        self.base = os.path.abspath(os.getcwd())
        # Create dirs: 'include' and 'lib'
        self.incdir = os.path.join(self.base, 'include')
        self.libdir = os.path.join(self.base, 'lib', self.ostype, self.arch)
        # Make sure dir for current dependency does not exist, otherwise exit if -f is not set
        if os.path.exists(self.libdir):
            if cli_params.force_rebuild:
                utils.remove_if_existent(self.libdir)
            else:
                print "Dependency already exists: name=%s version=%s, OS=%s, arch=%s" % \
                    (self.name(), self.swversion, self.ostype, self.arch)
                sys.exit()
        utils.create_dir_if_needed(self.incdir)
        utils.create_dir_if_needed(self.libdir)
        # Handle custom resolver parameters
        self.version_configurers = cr_params.version_configurers
        self.swversion = cr_params.latest_version if self.swversion == 'latest' else self.swversion
        if not self.swversion in self.version_configurers:
            raise ResolverError("Unsupported version: %s" % self.swversion)
        self.supported_os_types = cr_params.supported_os_types
        if not self.ostype in self.supported_os_types:
            raise abstract.ResolverError("Unsupported OS: %s" % self.ostype)

    def name(self):
        return self.name_impl()

    def name_impl(self):
        raise NotImplementedError

    def source(self):
        utils.begin_section("Bringing source for %s" % self.name())
        if utils.create_dir_if_needed('source'):
            os.chdir('source')
            self.source_impl()
        else:
            os.chdir(os.path.join('source', self.name_impl()))
        utils.end_section()

    def source_impl(self):
        raise NotImplementedError

    def config(self):
        utils.begin_section("Configuring %s for %s, %s" % (self.name(), self.ostype, self.arch))
        self.make_arguments = ''
        self.cflags = '-gdwarf-4'
        self.cxxflags = ''
        self.cppflags = '-DNDEBUG=1'
        self.ldflags = '-stdlib=libc++'
        self.cc = 'gcc'
        self.cxx = 'g++'
        self.libs = '-lc++ -lc++abi'
        if self.hostos == "Darwin":
            self.cppflags += ' -DINT_MAX=2147483647'
            self.cc = 'clang'
            self.cxx = 'clang'
        # Keep following call order
        self.os_configurers[self.ostype]()
        self.environmenters[self.ostype]()
        self.version_configurers[self.swversion]()
        utils.end_section()

    def make(self):
        utils.begin_section("Building %s" % self.name())
        self.make_impl()
        utils.end_section()

    def make_impl(self):
        subprocess.call('make clean', shell = True)
        subprocess.call('make -j4 %s' % self.make_arguments, shell = True)

    def finish(self):
        utils.begin_section("Finalizing installation of %s" % self.name())
        # Prepare include dir
        utils.remove_if_existent(self.incdir)
        # Prepare lib dir
        utils.remove_if_existent(self.libdir)
        # Do custom finish
        self.finish_impl()
        utils.end_section()

    def finish_impl():
        raise NotImplementedError

    def run(self):
        self.source()
        self.config()
        self.make()
        self.finish()

# OS-specific
    def get_machine_name(self):
        proc = subprocess.Popen('uname -m', shell = True, stdout = subprocess.PIPE)
        return proc.communicate()[0].strip()

    # todo: very macish --> make generic
    def config_local(self):
        min_sdk_version = '10.12'
        if self.arch == 'default':
            self.arch = self.get_machine_name()
        self.cflags = '%s -w -Os -flto -fPIC -mmacosx-version-min=%s' % (self.cflags, min_sdk_version)
        self.cxxflags = '%s -std=c++11 -stdlib=libc++' % self.cflags

    def config_ios_iphonesim(self, name_target):
        self.ios_min_sdk_version = '10.3'
        proc = subprocess.Popen('xcode-select --print-path', shell = True, stdout = subprocess.PIPE)
        xcode_dir = proc.communicate()[0].strip()
        sysroot = '%s/Platforms/%s.platform/Developer/SDKs/%s.sdk' % (xcode_dir, name_target, name_target)
        if self.arch == 'default':
            if name_target == 'iPhoneOS':
                self.arch = 'arm64'
            else:
                self.arch = self.get_machine_name()
        self.cflags = '%s -w -Os -flto -fPIC -fembed-bitcode -miphoneos-version-min=%s -arch %s -isysroot %s' % \
            (self.cflags, self.ios_min_sdk_version, self.arch, sysroot)
        self.cxxflags = '%s -std=c++11 -stdlib=libc++' % self.cflags
        self.ldflags = '%s -arch %s -miphoneos-version-min=%s' % (self.ldflags, self.arch, self.ios_min_sdk_version)
        self.cxxcpp = '%s/usr/bin/gcc -E' % xcode_dir
        self.ld = '%s/usr/bin/ld -ios_version_min %s' % (xcode_dir, self.ios_min_sdk_version)
        self.ldxx = '%s/usr/bin/ld -ios_version_min %s' % (xcode_dir, self.ios_min_sdk_version)
        self.cc = '%s/usr/bin/gcc' % (xcode_dir)
        self.cxx = '%s/usr/bin/g++' % (xcode_dir)

    def config_ios(self):
        self.config_ios_iphonesim('iPhoneOS')

    def config_iphone_sim(self):
        self.config_ios_iphonesim('iPhoneSimulator')

    def environment_local(self):
        os.environ['CFLAGS'] = self.cflags
        os.environ['CXXFLAGS'] = self.cxxflags
        os.environ['CPPFLAGS'] = self.cppflags

    def environment_ios(self):
        os.environ['CFLAGS'] = self.cflags
        os.environ['CXXFLAGS'] = self.cxxflags
        os.environ['CPPFLAGS'] = self.cppflags
        os.environ['CXXCPP'] = self.cxxcpp
        os.environ['LD'] = self.ld
        os.environ['LDXX'] = self.ldxx
        os.environ['CC'] = self.cc
        os.environ['CXX'] = self.cxx

    def environment_iphone_sim(self):
        self.environment_ios()

# Internal utilities
    def add_prefix_dir(self):
        self.prefix = os.path.join(self.base, 'prefix')
        utils.remove_if_existent(self.prefix)
        utils.create_dir_if_needed(self.prefix)

    def copy_executables_if_needed(self, src_path):
        # Copy executables only for local build
        if os.path.exists(src_path) and self.ostype == 'local':
            bindir = os.path.join(self.base, 'bin')
            utils.remove_if_existent(bindir)
            shutil.copytree(src_path, bindir)

    def git_clone_and_checkout(self, git_url):
        subprocess.call('git clone %s %s' % (git_url, self.name_impl()), shell = True)
        os.chdir(self.name_impl())
        subprocess.call(['git', 'checkout', self.swversion])

    def archive_dwl_and_unpack(self, archive_url, unpacked_dir_name):
        archive_name = re.sub(r'(.*)/([^/]+)', r'\2', archive_url)
        subprocess.call('wget -O %s %s' % (archive_name, archive_url), shell = True)
        if re.match(r'.*\.tar(\.[gbx]z2?)?', archive_name):
            subprocess.call('tar -xf %s' % archive_name, shell = True)
            os.remove(archive_name)
            # Rename the unpacked dir to name_impl()
            new_dir_name = os.path.join('.', self.name_impl())
            os.rename(os.path.join('.', unpacked_dir_name), new_dir_name)
            os.chdir(new_dir_name)

    def finish_from_prefix(self):
        subprocess.call('make install', shell = True)
        os.chdir(os.path.join('..', '..'))
        self.copy_executables_if_needed(os.path.join('prefix', 'bin'))
        shutil.copytree(os.path.join('prefix', 'include'), self.incdir)
        shutil.copytree(os.path.join('prefix', 'lib'), self.libdir)
        utils.remove_if_existent('prefix')
