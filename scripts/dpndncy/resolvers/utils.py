import errno
import os
import shutil

# Colors:
#  export COLOR_NC='\e[0m' # No Color
#  export COLOR_WHITE='\e[1;37m'
#  export COLOR_BLACK='\e[0;30m'
#  export COLOR_BLUE='\e[0;34m'
#  export COLOR_LIGHT_BLUE='\e[1;34m'
#  export COLOR_GREEN='\e[0;32m'
#  export COLOR_LIGHT_GREEN='\e[1;32m'
#  export COLOR_CYAN='\e[0;36m'
#  export COLOR_LIGHT_CYAN='\e[1;36m'
#  export COLOR_RED='\e[0;31m'
#  export COLOR_LIGHT_RED='\e[1;31m'
#  export COLOR_PURPLE='\e[0;35m'
#  export COLOR_LIGHT_PURPLE='\e[1;35m'
#  export COLOR_BROWN='\e[0;33m'
#  export COLOR_YELLOW='\e[1;33m'
#  export COLOR_GRAY='\e[0;30m'
#  export COLOR_LIGHT_GRAY='\e[0;37m'

def highlight(string, status, bold):
    attr = []
    if status == 1: # green
        attr.append('32')
    elif status == 2: # red
        attr.append('31')
    elif status == 3: # cyan
        attr.append('36')
    else: # none
        pass
    if bold:
        attr.append('1')
    return '\x1b[%sm%s\x1b[0m' % (';'.join(attr), string)

def begin_section(string):
    l = len(string)
    print highlight("+-%s-+" % ("-" * l), 1, True)
    print highlight("| %s |" % string, 1, True)
    print highlight("+-%s-+" % ("-" * l), 1, True)

def end_section():
    print highlight('Done.', 3, True)

def create_dir_if_needed(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
        return True
    return False

def remove_if_existent(directory):
    if os.path.exists(directory):
        shutil.rmtree(directory)

def copy_anything(src, dst):
    try:
        shutil.copytree(src, dst)
    except OSError as exc:
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else:
            raise
