import abstract
import os

#---------------------------------------#
# Common utilities for gRPC & protobuf. #
#---------------------------------------#
class GrpcProtobufCommons(object):
    def handle_plugins(self):
        self.plugins_path = os.path.join(self.base, 'bin')
        if self.ostype != 'local' and not os.path.exists(self.plugins_path):
            raise abstract.ResolverError("Plugins not available, please build first for OS=\'local\'")
