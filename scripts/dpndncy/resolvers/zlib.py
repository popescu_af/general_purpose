import abstract
import subprocess

#---------------------------#
# Custom resolver for zlib. #
#---------------------------#
class CustomResolver(abstract.Resolver):
    def __init__(self, cli_params):
        resolver_params = type('', (), {})()
        resolver_params.version_configurers = {'1.2.8':self.config_v_1_2_8}
        resolver_params.latest_version = '1.2.8'
        resolver_params.supported_os_types = ['local', 'iOS', 'iPhoneSimulator']
        super(CustomResolver, self).__init__(cli_params, resolver_params)
        # Others
        self.add_prefix_dir()

    def name_impl(self):
        return 'zlib'

    def source_impl(self):
        self.archive_dwl_and_unpack('https://zlib.net/fossils/zlib-%s.tar.gz' % self.swversion, \
                                    'zlib-%s' % self.swversion)

    def config_v_1_2_8(self):
        # Configure
        subprocess.call('./configure --prefix=%s' % self.prefix, shell = True)

    def finish_impl(self):
        self.finish_from_prefix()
