import abstract
import os
import shutil
import subprocess
import utils

import gRPC_proto_common

#---------------------------#
# Custom resolver for gRPC. #
#---------------------------#
class CustomResolver(abstract.Resolver, gRPC_proto_common.GrpcProtobufCommons):
    def __init__(self, cli_params):
        resolver_params = type('', (), {})()
        resolver_params.version_configurers = {'v1.6.7':self.config_v_1_6_7}
        resolver_params.latest_version = 'v1.6.7'
        resolver_params.supported_os_types = ['local', 'iOS', 'iPhoneSimulator']
        super(CustomResolver, self).__init__(cli_params, resolver_params)
        # Others
        self.handle_plugins()

    def name_impl(self):
        return 'gRPC'

    def source_impl(self):
        self.git_clone_and_checkout('https://github.com/grpc/grpc.git')
        subprocess.call('git submodule update --init', shell = True)

    def config_v_1_6_7(self):
        subprocess.call('git checkout -- Makefile', shell = True)
        if self.ostype == 'iOS' or self.ostype == 'iPhoneSimulator':
            # Set required environment variables
            os.environ['GRPC_CROSS_COMPILE'] = '1'
            if self.ostype == 'iPhoneSimulator':
                os.environ['PROTOBUF_CONFIG_OPTS'] = '--host=x86_64'
            os.environ['HAS_PKG_CONFIG'] = '0'
            # Remove unknown flags to Apple's linker
            subprocess.call('sed -i \'\' \'s/ -g / /\' Makefile', shell = True)
            subprocess.call('sed -i \'\' \'s/^LDFLAGS += -fPIC/LDFLAGS += -flto/\' Makefile', shell = True)
            # Don't build gRPC plugins, use the ones installed in the system instead!
            subprocess.call('sed -i \'\' \'s/^plugins/#plugins/\' Makefile', shell = True)
            subprocess.call('sed -i \'\' \'s/^PROTOC_PLUGINS_ALL = /#PROTOC_PLUGINS_ALL = /\' Makefile', shell = True)
            safe_plugins_path = self.plugins_path.replace('/', '\/')
            subprocess.call('sed -E -i \'\' \'s/^(PROTOC_PLUGINS_DIR = )(.*)/\\1%s/\' Makefile' % safe_plugins_path, shell = True)
            # Do not build reflection lib and cronet lib, not needed.
            subprocess.call('sed -E -i \'\' \'s/(static_cxx: pc_cxx pc_cxx_unsecure cache.mk  )(.*libgrpc\+\+.a )(.*cronet.a )(.*reflection.a )(.*unsecure.a)/\\1\\2\\5/\' Makefile', shell = True)
            # Do not merge OPENSSL into GRPC, it causes duplicate symbols at linking time.
            subprocess.call('sed -i \'\' \'s/^OPENSSL_MERGE_OBJS/#OPENSSL_MERGE_OBJS/\' Makefile', shell = True)
            self.make_arguments = 'static'

    def finish(self):
        self.copy_executables_if_needed(os.path.join('bins', 'opt'))
        utils.remove_if_existent(self.incdir)
        utils.remove_if_existent(self.libdir)
        shutil.copytree('./include', self.incdir)
        shutil.copytree('./third_party/boringssl/include/openssl', os.path.join(self.incdir, 'openssl'))
        shutil.copytree('./libs/opt', self.libdir)
