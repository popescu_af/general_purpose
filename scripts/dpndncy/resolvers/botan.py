import abstract
import subprocess

#----------------------------#
# Custom resolver for botan. #
#----------------------------#
class CustomResolver(abstract.Resolver):
    def __init__(self, cli_params):
        resolver_params = type('', (), {})()
        resolver_params.version_configurers = {'2.3.0':self.config_v_2_3_0}
        resolver_params.latest_version = '2.3.0'
        resolver_params.supported_os_types = ['local', 'iOS', 'iPhoneSimulator']
        super(CustomResolver, self).__init__(cli_params, resolver_params)
        # Others
        self.add_prefix_dir()

    def name_impl(self):
        return 'botan'

    def source_impl(self):
        self.git_clone_and_checkout('https://github.com/randombit/botan.git')

    def config_v_2_3_0(self):
        command_line_args = 'python ./configure.py --optimize-for-size --prefix=%s' % self.prefix
        if self.ostype == 'iOS':
            command_line_args += ' --disable-shared --os=ios --cpu=armv8-a --cc-abi-flags="-arch arm64" --cc=clang'
        elif self.ostype == 'iPhoneSimulator':
            command_line_args += ' --disable-shared --os=ios --cpu=x86_64 --cc-abi-flags="-arch x86_64" --cc=clang'
        subprocess.call(command_line_args, shell = True)

    def make_impl(self):
        if self.ostype == 'iOS':
            subprocess.call('xcrun --sdk iphoneos make install', shell = True)
        elif self.ostype == 'iPhoneSimulator':
            subprocess.call('xcrun --sdk iphonesimulator make install', shell = True)
        else:
            subprocess.call('make clean', shell = True)
            subprocess.call('make -j4 %s' % self.make_arguments, shell = True)

    def finish_impl(self):
        self.finish_from_prefix()
