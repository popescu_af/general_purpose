import abstract
import os
import subprocess
import utils

#----------------------------#
# Custom resolver for boost. #
#----------------------------#
class CustomResolver(abstract.Resolver):
    def __init__(self, cli_params):
        resolver_params = type('', (), {})()
        resolver_params.version_configurers = {'boost-1.65.1':self.config_v_1_65_1}
        resolver_params.latest_version = 'boost-1.65.1'
        resolver_params.supported_os_types = ['local', 'iOS', 'iPhoneSimulator']
        super(CustomResolver, self).__init__(cli_params, resolver_params)

    def name_impl(self):
        return 'boost'

    def source_impl(self):
        self.git_clone_and_checkout('https://github.com/boostorg/boost.git')
        subprocess.call('git submodule update --init', shell = True)

    def generate_cmake_file(self):
        # todo: actually take into consideration the modules list and generate the cmake file accordingly
        cmake_lines = [
            'cmake_minimum_required(VERSION 3.5 FATAL_ERROR)\n',
            'project(boost)\n',
            'file(GLOB SYSTEM_SRC "${CMAKE_SOURCE_DIR}/libs/system/src/*.*pp")\n',
            'add_library(${PROJECT_NAME}_system STATIC ${SYSTEM_SRC})\n',
            'target_include_directories(${PROJECT_NAME}_system SYSTEM\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/assert/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/config/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/core/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/predef/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/system/include\n',
            ')\n',
            'file(GLOB FILESYSTEM_SRC "${CMAKE_SOURCE_DIR}/libs/filesystem/src/*.*pp")\n',
            'add_library(${PROJECT_NAME}_filesystem STATIC ${FILESYSTEM_SRC})\n',
            'target_include_directories(${PROJECT_NAME}_filesystem SYSTEM\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/detail/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/filesystem/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/functional/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/io/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/iterator/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/mpl/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/range/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/smart_ptr/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/static_assert/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/preprocessor/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/throw_exception/include\n',
            '    PUBLIC ${CMAKE_SOURCE_DIR}/libs/type_traits/include\n',
            ')\n',
            'target_link_libraries(${PROJECT_NAME}_filesystem ${PROJECT_NAME}_system)\n'
            ]
        fh = open("./CMakeLists.txt", "w")
        fh.writelines(cmake_lines)
        fh.close()

    def merge_recursively(self, src, dst):
        for entry in os.listdir(src):
            src_entry = os.path.join(src, entry)
            dst_entry = os.path.join(dst, entry)
            if os.path.exists(dst_entry):
                if os.path.isdir(dst_entry):
                    self.merge_recursively(src_entry, dst_entry)
            else:
                utils.copy_anything(src_entry, dst_entry)

    def copy_headers(self):
        header_dest = os.path.join(self.incdir, 'boost')
        utils.create_dir_if_needed(header_dest)

        boost_libs = os.path.join('.', 'libs')
        directories = [e for e in os.listdir(boost_libs) if os.path.isdir(os.path.join(boost_libs, e))]

        for d in directories:
            header_src = os.path.join(boost_libs, d, 'include', 'boost')
            if os.path.isdir(header_src):
                print "Copying headers:", header_src, "->", header_dest
                self.merge_recursively(header_src, header_dest)

    def config_v_1_65_1(self):
        self.generate_cmake_file()
        utils.remove_if_existent('build')
        utils.create_dir_if_needed('build')
        os.chdir('build')
        subprocess.call('cmake -G "Unix Makefiles" ..', shell = True)

    def finish_impl(self):
        utils.create_dir_if_needed(self.libdir)
        os.rename('./libboost_filesystem.a', os.path.join(self.libdir, 'libboost_filesystem.a'))
        os.rename('./libboost_system.a', os.path.join(self.libdir, 'libboost_system.a'))
        os.chdir('..')
        self.copy_headers()
