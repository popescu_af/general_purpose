import abstract
import os
import subprocess

import gRPC_proto_common

#-------------------------------#
# Custom resolver for protobuf. #
#-------------------------------#
class CustomResolver(abstract.Resolver, gRPC_proto_common.GrpcProtobufCommons):
    def __init__(self, cli_params):
        resolver_params = type('', (), {})()
        resolver_params.version_configurers = {'v3.4.0':self.config_v_3_4_0}
        resolver_params.latest_version = 'v3.4.0'
        resolver_params.supported_os_types = ['local', 'iOS', 'iPhoneSimulator']
        super(CustomResolver, self).__init__(cli_params, resolver_params)
        # Others
        self.handle_plugins()
        self.add_prefix_dir()

    def name_impl(self):
        return 'protobuf'

    def source_impl(self):
        self.git_clone_and_checkout('https://github.com/google/protobuf.git')

    def config_v_3_4_0(self):
        subprocess.call('bash ./autogen.sh', shell = True)
        subprocess.call('make distclean', shell = True)
        # Configure
        common_config = 'CC=\"%s\" CFLAGS=\"%s\" CXX=\"%s\" CXXFLAGS=\"%s\" CPPFLAGS=\"%s\" LDFLAGS=\"%s\" LIBS=\"%s\"' % \
                        (self.cc, self.cflags, self.cxx, self.cxxflags, self.cppflags, self.ldflags, self.libs)
        if self.ostype == 'local':
            subprocess.call('./configure --prefix=%s %s' % (self.prefix, common_config), shell = True)
        elif self.ostype == 'iOS' or self.ostype == 'iPhoneSimulator':
            proc = subprocess.Popen('gcc -dumpmachine', shell = True, stdout = subprocess.PIPE)
            current_machine = proc.communicate()[0].strip()
            preprocessor_config = 'CPP=\"%s\" CXXCPP=\"%s\"' % (self.cxxcpp, self.cxxcpp)
            host = 'arm' if self.ostype == 'iOS' else 'x86_64'
            subprocess.call('./configure --build=%s --host=%s --with-protoc=%s --disable-shared --prefix=%s %s %s' % \
                            (current_machine, host, os.path.join(self.plugins_path, 'protoc'), self.prefix, common_config, preprocessor_config), shell = True)

    def finish_impl(self):
        self.finish_from_prefix()
