import abstract
import os
import subprocess

#------------------------------#
# Custom resolver for libcurl. #
#------------------------------#
class CustomResolver(abstract.Resolver):
    def __init__(self, cli_params):
        resolver_params = type('', (), {})()
        resolver_params.version_configurers = {'7.47.1':self.config_v_7_47_1}
        resolver_params.latest_version = '7.47.1'
        resolver_params.supported_os_types = ['local', 'iOS', 'iPhoneSimulator']
        super(CustomResolver, self).__init__(cli_params, resolver_params)
        # Others
        self.add_prefix_dir()

    def name_impl(self):
        return 'libcurl'

    def source_impl(self):
        self.archive_dwl_and_unpack('https://curl.haxx.se/download/curl-%s.tar.gz' % self.swversion, \
                                    'curl-%s' % self.swversion)

    def config_v_7_47_1(self):
        # Generate configure if needed
        if not os.path.exists(os.path.join('.', 'configure')):
            subprocess.call('./buildconf', shell = True)
        # Extra CFLAGS
        self.cflags += " -Werror=partial-availability"
        # Configure
        common_config = '--with-darwinssl --enable-static --disable-shared --enable-threaded-resolver --disable-verbose'
        common_config = '%s --enable-ipv6 --prefix=%s' % (common_config, self.prefix)
        if self.ostype == 'local':
            subprocess.call('./configure %s' % common_config, shell = True)
        elif self.ostype == 'iOS' or self.ostype == 'iPhoneSimulator':
            host = 'arm' if self.ostype == 'iOS' else 'x86_64'
            subprocess.call('./configure --host=%s-apple-darwin %s CPP=\"%s\" CXXCPP=\"%s\"' % \
                            (host, common_config, self.cxxcpp, self.cxxcpp), shell = True)

    def finish_impl(self):
        self.finish_from_prefix()
