#
# Copyright (c) 2016 - 2018, Alexandru Florinel Popescu
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies,
# either expressed or implied, of the FreeBSD Project.
#

function(add_default_libraries TARGET_NAME)
#{
    find_library(COCOA      Cocoa)
    find_library(CORE_VIDEO CoreVideo)

    target_link_libraries(${TARGET_NAME}
        ${COCOA}
        ${CORE_VIDEO}
    )
#}
endfunction()

macro(add_3rdparty_module NAME TARGET)
#{
    find_path(${NAME}_INCLUDE_DIR NAMES ${NAME}.h)
    find_library(${NAME}_LIBRARIES NAMES ${NAME})

    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(${NAME} DEFAULT_MSG ${NAME}_LIBRARIES ${NAME}_INCLUDE_DIR)

    mark_as_advanced(${NAME}_INCLUDE_DIR ${NAME}_LIBRARIES)

    if(NOT EXISTS ${${NAME}_INCLUDE_DIR})
    #{
        message(FATAL_ERROR "${NAME} include dir not found: ${${NAME}_INCLUDE_DIR}")
    #}
    endif()

    if(NOT EXISTS ${${NAME}_LIBRARIES})
    #{
        message(FATAL_ERROR "${NAME} libraries not found: ${${NAME}_LIBRARIES}")
    #}
    endif()

    target_include_directories(${TARGET} SYSTEM PUBLIC ${${NAME}_INCLUDE_DIR})
    target_link_libraries(${TARGET} ${${NAME}_LIBRARIES})
#}
endmacro()

macro(add_prebuilt_module NAME TARGET)
#{
    target_link_libraries(${TARGET} ${DEPENDENCIES_DIRECTORY}/lib/lib${NAME}.a)
#}
endmacro()

function(add_boost TARGET_NAME)
#{
    include(FindBoost)
    find_package(Boost COMPONENTS chrono filesystem system thread REQUIRED)

    set(Boost_LIBRARIES
        ${Boost_CHRONO_LIBRARY}
        ${Boost_FILESYSTEM_LIBRARY}
        ${Boost_SYSTEM_LIBRARY}
        ${Boost_THREAD_LIBRARY}
    )

    target_include_directories(${TARGET_NAME} SYSTEM PUBLIC ${Boost_INCLUDE_DIRS})
    target_link_libraries(${TARGET_NAME} ${Boost_LIBRARIES})
#}
endfunction()

function(add_ffmpeg TARGET_NAME)
#{
    target_include_directories(${TARGET_NAME} SYSTEM PUBLIC /usr/local/include)
    target_link_libraries(${TARGET_NAME}
        /usr/local/lib/libavcodec.dylib
        /usr/local/lib/libavdevice.dylib
        /usr/local/lib/libavfilter.dylib
        /usr/local/lib/libavformat.dylib
        /usr/local/lib/libavutil.dylib
        /usr/local/lib/libavresample.dylib
        /usr/local/lib/libswscale.dylib
        /usr/local/lib/libswresample.dylib
        /usr/local/lib/libpostproc.dylib
    )
#}
endfunction()

function(add_protobuf TARGET_NAME)
#{
    include(FindProtobuf)
    find_package(Protobuf REQUIRED)

    target_include_directories(${TARGET_NAME} SYSTEM
        PUBLIC ${PROTOBUF_INCLUDE_DIR}
    )

    target_link_libraries(${TARGET_NAME}
        ${PROTOBUF_LIBRARIES}
    )
#}
endfunction()

function(add_openssl TARGET_NAME)
#{
    include(FindOpenSSL)
    find_package(OpenSSL REQUIRED)

    target_include_directories(${TARGET_NAME} SYSTEM
        PUBLIC ${OPENSSL_INCLUDE_DIR}
    )

    target_link_libraries(${TARGET_NAME}
        ${OPENSSL_LIBRARIES}
    )
#}
endfunction()

function(add_libcurl TARGET_NAME)
#{
    include(FindCURL)
    find_package(CURL REQUIRED)

    target_include_directories(${TARGET_NAME} SYSTEM
        PUBLIC ${CURL_INCLUDE_DIRS}
    )

    target_link_libraries(${TARGET_NAME}
        ${CURL_LIBRARIES}
    )
#}
endfunction()

function(add_gtest TARGET_NAME)
#{
    target_include_directories(${TARGET_NAME} SYSTEM
        PUBLIC ${DEPENDENCIES_DIRECTORY}/googletest/googlemock/include
        PUBLIC ${DEPENDENCIES_DIRECTORY}/googletest/googletest/include
    )

    target_link_libraries(${TARGET_NAME} gmock gtest)
#}
endfunction()

function(add_libkml TARGET_NAME)
#{
    include(FindLibKML)
    find_package(LibKML REQUIRED)

    target_include_directories(${TARGET_NAME} SYSTEM PUBLIC ${LIBKML_INCLUDE_DIRS})
    target_link_libraries(${TARGET_NAME} ${LIBKML_LIBRARIES})
#}
endfunction()

function(add_soci TARGET_NAME)
#{
    include(FindSOCI)
    find_package(SOCI REQUIRED)

    target_include_directories(${TARGET_NAME} SYSTEM
        PUBLIC ${SOCI_INCLUDE_DIR}
    )

    target_link_libraries(${TARGET_NAME}
        ${SOCI_LIBRARY}
    )
#}
endfunction()

function(target_connect_3rdparty)
#{
    set(options)
    set(oneValueArgs TARGET)
    set(multiValueArgs 3RDPARTY PREBUILT)
    cmake_parse_arguments(CONN "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    foreach(DEP IN ITEMS ${CONN_3RDPARTY})
    #{
        if("${DEP}" STREQUAL "default")
        #{
            add_default_libraries(${CONN_TARGET})
        #}
        elseif("${DEP}" STREQUAL "boost")
        #{
            add_boost(${CONN_TARGET})
        #}
        elseif("${DEP}" STREQUAL "ffmpeg")
        #{
            add_ffmpeg(${CONN_TARGET})
        #}
        elseif("${DEP}" STREQUAL "protobuf")
        #{
            add_protobuf(${CONN_TARGET})
        #}
        elseif("${DEP}" STREQUAL "openssl")
        #{
            add_openssl(${CONN_TARGET})
        #}
        elseif("${DEP}" STREQUAL "libcurl")
        #{
            add_libcurl(${CONN_TARGET})
        #}
        elseif("${DEP}" STREQUAL "gtest")
        #{
            add_gtest(${CONN_TARGET})
        #}
        elseif("${DEP}" STREQUAL "soci")
        #{
            add_soci(${CONN_TARGET})
        #}
        elseif("${DEP}" STREQUAL "libkml")
        #{
            add_libkml(${CONN_TARGET})
        #}
        elseif("${DEP}" STREQUAL "zlib")
        #{
            target_link_libraries(${CONN_TARGET} z)
        #}
        else()
        #{
            add_3rdparty_module(${DEP} ${CONN_TARGET})
        #}
        endif()
    #}
    endforeach()

    foreach(DEP IN ITEMS ${CONN_PREBUILT})
    #{
        add_prebuilt_module(${DEP} ${CONN_TARGET})
    #}
    endforeach()
#}
endfunction()

