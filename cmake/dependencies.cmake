#
# Copyright (c) 2016 - 2018, Alexandru Florinel Popescu
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies,
# either expressed or implied, of the FreeBSD Project.
#

function(add_build_dependency URL ADD_SUBDIR INHERIT_TOOLCHAINS)
#{
	if(NOT ${DEPENDENCIES_DIRECTORY} OR NOT EXISTS ${DEPENDENCIES_DIRECTORY})
	#{
		set(DEPENDENCIES_DIRECTORY "${CMAKE_BINARY_DIR}/dependencies" CACHE PATH "Path to dependencies directory.")
		file(MAKE_DIRECTORY ${DEPENDENCIES_DIRECTORY})
	#}
	endif()

    string(REGEX REPLACE "(.*)/(.*)\\.git" "\\2" NAME ${URL})

    execute_process(COMMAND git clone ${URL} WORKING_DIRECTORY ${DEPENDENCIES_DIRECTORY})

    if(NOT IS_DIRECTORY ${DEPENDENCIES_DIRECTORY}/${NAME})
        execute_process(COMMAND git pull WORKING_DIRECTORY ${DEPENDENCIES_DIRECTORY}/${NAME})
    endif()

    if(${ADD_SUBDIR})
        add_subdirectory(${DEPENDENCIES_DIRECTORY}/${NAME} ${DEPENDENCIES_DIRECTORY}/build/${NAME})
    endif()

    if(${INHERIT_TOOLCHAINS})
        set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${DEPENDENCIES_DIRECTORY}/${NAME}/cmake/" PARENT_SCOPE)
    endif()
#}
endfunction()


