#
# Copyright (c) 2016 - 2018, Alexandru Florinel Popescu
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are those
# of the authors and should not be interpreted as representing official policies,
# either expressed or implied, of the FreeBSD Project.
#

function(target_default_compiler_flags)
#{
    set(options)
    set(oneValueArgs TARGET)
    set(multiValueArgs)
    cmake_parse_arguments(CPP "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    if("${CMAKE_CXX_COMPILER_ID}" MATCHES "^Clang|AppleClang$")
    #{
        target_compile_options(${CPP_TARGET}
            PUBLIC -Weverything
            PUBLIC -Wno-missing-variable-declarations
            PUBLIC -Wno-missing-prototypes
            PUBLIC -Wno-reserved-id-macro
            PUBLIC -Wno-implicit-fallthrough
            PUBLIC -Wno-c++98-compat
            PUBLIC -Wno-c++98-compat-pedantic
            PUBLIC -Wno-c99-extensions
            PUBLIC -Wno-global-constructors
            PUBLIC -Wno-exit-time-destructors
            PUBLIC --system-header-prefix=boost/
            PUBLIC --system-header-prefix=wrap_3rdparty/
            PUBLIC --system-header-prefix=gtest/
            PUBLIC --system-header-prefix=google/
        )
    #}
    elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    #{
        target_compile_options(${CPP_TARGET}
            PUBLIC -Wall
        )
    #}
    endif()

    target_compile_options(${CPP_TARGET}
        PUBLIC -std=c++14
        PUBLIC -Wextra
        PUBLIC -Werror
        PUBLIC -pedantic
        PUBLIC -Wunused-parameter
        PUBLIC -Wsign-conversion
        PUBLIC -Wold-style-cast
        PUBLIC -Wno-variadic-macros
        PUBLIC -Wno-switch-enum
        PUBLIC -Wno-padded
        PUBLIC -Wno-system-headers
        PUBLIC -Wno-unused-macros
        PUBLIC -fvisibility=hidden
    )

    target_include_directories(${CPP_TARGET} SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/3rdparty/include)
    target_include_directories(${CPP_TARGET} SYSTEM PUBLIC ${CMAKE_SOURCE_DIR}/include)
#}
endfunction()

