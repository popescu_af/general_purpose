/**
 *
 * Copyright (c) 2016, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#include <gtest/gtest.h>

#include <bonjour/browser.h>
#include <bonjour/registrar.h>
#include <bonjour/resolver.h>

#include <memory>

#define TEST_PORT 50050

using namespace gp_bonjour;

Record test_record()
{
    return Record("lbB16B00B5", "_lockdboxB16B00B5._tcp", "");
}

std::unique_ptr<Registrar> create_and_validate_registrar(const Record& record)
{
    std::unique_ptr<Registrar> registrar(new Registrar());
    auto result = registrar->register_service(record, TEST_PORT);
    auto err = result.get();
    EXPECT_EQ(err, kDNSServiceErr_NoError);
    return registrar;
}

TEST(GP_BONJOUR, test_registrar)
{
    Record record = test_record();
    auto registrar = create_and_validate_registrar(record);
    auto registered_record = registrar->registered_record();
    ASSERT_EQ(registered_record.service_name_, record.service_name_);
    ASSERT_EQ(registered_record.type_, record.type_ + ".");
    ASSERT_EQ(registered_record.domain_, "local.");
}

using namespace std::chrono_literals;

class Callbacks
{
public:
    void records_changed()
    {
        std::unique_lock<std::mutex> lock(mtx_);
        cv_.notify_one();
    }

    bool wait_for_record_and_stop()
    {
        std::unique_lock<std::mutex> lock(mtx_);
        return (cv_.wait_for(lock, 5s) == std::cv_status::no_timeout);
    }

private:
    std::mutex mtx_;
    std::condition_variable cv_;
};

TEST(GP_BONJOUR, test_browser_and_resolver)
{
    Record expected_service = test_record();
    auto registrar = create_and_validate_registrar(expected_service);

    Callbacks cbs;
    Browser browser;
    browser.set_records_changed_callback([&cbs]{ cbs.records_changed(); });

    auto wait_for_result = std::async(std::launch::async,
                                      [&cbs]()
                                      {
                                          return cbs.wait_for_record_and_stop();
                                      });

    // Give some execution advantage to the async thread, to enter waiting state.
    std::this_thread::sleep_for(100ms);

    ASSERT_EQ(browser.browse_services_of_type(expected_service.type_), kDNSServiceErr_NoError);
    ASSERT_TRUE(wait_for_result.get());
    browser.stop_browsing();

    ASSERT_EQ(browser.records().size(), 1);

    Resolver resolver;
    auto result = resolver.resolve_record(browser.records().front());
    result.wait();

    ASSERT_EQ(resolver.get_port(), TEST_PORT);
}

TEST(GP_BONJOUR, hostname)
{
    auto hostname = get_hostname();
    std::cout << "Bonjour hostname is: " << hostname << "\n";
    ASSERT_NE(hostname, "");
}

int main(int argc, char* argv[])
{
    ::testing::GTEST_FLAG(filter) = "*.*";

    testing::InitGoogleTest(&argc, argv);
    testing::AddGlobalTestEnvironment(new testing::Environment());

    return RUN_ALL_TESTS();
}
