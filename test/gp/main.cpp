/**
 *
 * Copyright (c) 2016, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#include <stdlib.h>

#include <gtest/gtest.h>

#include <ds/rotating_matrix.h>

#include <gp/connection.h>

#include <gp/gp.h>

TEST(DS, matrix_rotation_test)
{
    //
    // See: https://www.hackerrank.com/challenges/matrix-rotation-algo
    //

    // TODO: make test out of this
    //    uint32_t rows, cols, rotations;
    //    std::cin >> rows >> cols >> rotations;
    //
    //    std::vector<uint32_t> elements(rows * cols, 0);
    //
    //    for (uint32_t i = 0; i < rows; ++i)
    //    {
    //        for (uint32_t j = 0; j < cols; ++j)
    //        {
    //            std::cin >> elements[i * cols + j];
    //        }
    //    }
    //
    //    ds::RotMatrix<uint32_t> m(rows, cols, elements);
    //    m.rotate(rotations);
    //
    //    std::cout << m;
}

using namespace gp::inet;
const std::string TEST_HOST = "localhost";
const Port TEST_PORT = 6666; // Random test port

std::string hello_world() { return "Hello world!"; }

class HelloWorldServer : public gp::inet::AbstractServer
{
  public:
    virtual void serve();
};

void HelloWorldServer::serve() { endpoint_->queued_send(hello_world()); }

TEST(GP_CONNECTION, test_client_server_hello_world)
{
    gp::inet::AbstractServer::pointer hw_server(new HelloWorldServer);
    TcpServer server(hw_server);
    server.start(TEST_PORT);

    gp::ErrorCode error_code;
    TcpClient client;
    client.connect(TEST_HOST, TEST_PORT, error_code);
    EXPECT_EQ(error_code.value(), 0) << error_code.message();

    std::string result_from_server;
    client.recv(result_from_server, error_code, hello_world().size());
    EXPECT_EQ(error_code.value(), 0);
    EXPECT_EQ(result_from_server, hello_world());

    client.disconnect();
    server.stop();
}

const uint32_t PACKETS_TOTAL = 16;

class SendPacketOrderTestServer : public gp::inet::AbstractServer
{
  public:
    virtual void serve();
};

void SendPacketOrderTestServer::serve()
{
    for (uint32_t i = 0; i < PACKETS_TOTAL; ++i)
    {
        endpoint_->queued_send(std::to_string(i));
    }
}

TEST(GP_CONNECTION, test_server_packet_order)
{
    gp::inet::AbstractServer::pointer spo_server(new SendPacketOrderTestServer);
    TcpServer server(spo_server);
    server.start(TEST_PORT);

    gp::ErrorCode error_code;
    TcpClient client;
    client.connect(TEST_HOST, TEST_PORT, error_code);
    EXPECT_EQ(error_code.value(), 0) << error_code.message();

    for (uint32_t i = 0; i < PACKETS_TOTAL; ++i)
    {
        const std::string expected = std::to_string(i);
        std::string result;
        const size_t size = expected.size();
        client.recv(result, error_code, size);
        EXPECT_EQ(result, expected);
    }

    client.disconnect();
    server.stop();
}

class RecvBufferOverflowTestServer : public gp::inet::AbstractServer
{
  public:
    virtual void serve();

    static const uint32_t BIG_PACKET_SIZE = 16380;
    static gp::ErrorCode& err()
    {
        static gp::ErrorCode e;
        return e;
    }
    static std::string message() { return "Hello"; }
    static std::string expected_message() { return "World!"; }
};

void RecvBufferOverflowTestServer::serve()
{
    const std::string big_packet(BIG_PACKET_SIZE, '@');
    endpoint_->queued_send(big_packet);

    std::string ack;
    endpoint_->recv(ack, err(), BIG_PACKET_SIZE);
    if (ack[0] != '#' || ack[BIG_PACKET_SIZE - 1] != '#')
    {
        GP_SET_ERROR(err(), bad_message);
        return;
    }

    endpoint_->queued_send(message());
    endpoint_->recv(ack, err(), expected_message().size());
    if (ack != expected_message())
    {
        GP_SET_ERROR(err(), bad_message);
        return;
    }

    endpoint_->queued_send(".");
}

TEST(GP_CONNECTION, test_recv_buffer_overflow)
{
    gp::inet::AbstractServer::pointer rbo_server(new RecvBufferOverflowTestServer);
    TcpServer server(rbo_server);
    server.start(TEST_PORT);

    gp::ErrorCode error_code;
    TcpClient client;
    client.connect(TEST_HOST, TEST_PORT, error_code);
    EXPECT_EQ(error_code.value(), 0) << error_code.message();

    std::string server_msg;
    client.recv(server_msg, error_code, RecvBufferOverflowTestServer::BIG_PACKET_SIZE);
    EXPECT_EQ(error_code.value(), 0) << error_code.message();
    EXPECT_EQ(server_msg[0], '@');
    EXPECT_EQ(server_msg[RecvBufferOverflowTestServer::BIG_PACKET_SIZE - 1], '@');

    const std::string big_packet(RecvBufferOverflowTestServer::BIG_PACKET_SIZE, '#');
    client.queued_send(big_packet);

    client.recv(server_msg, error_code, RecvBufferOverflowTestServer::message().size());
    EXPECT_EQ(server_msg, RecvBufferOverflowTestServer::message());

    client.queued_send(RecvBufferOverflowTestServer::expected_message());

    client.recv(server_msg, error_code, 1);
    EXPECT_EQ(server_msg, ".");
    EXPECT_EQ(RecvBufferOverflowTestServer::err().value(), 0) << RecvBufferOverflowTestServer::err().message();

    client.disconnect();
    server.stop();
}

int main(int argc, char* argv[])
{
    ::testing::GTEST_FLAG(filter) = "*.*";

    testing::InitGoogleTest(&argc, argv);
    testing::AddGlobalTestEnvironment(new testing::Environment());

    return RUN_ALL_TESTS();
}
