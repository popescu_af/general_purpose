//
// See: https://www.hackerrank.com/challenges/array-and-simple-queries
//

#include <iostream>
#include <ds/implicit_treap.h>

using namespace ds;
using namespace std;

void print_treap(ImplicitTreapPtr treap, bool print_diff = false)
{
    if (treap)
    {
        std::vector<int> result;
        treap->inorder(result);

        if (print_diff)
            std::cout << abs(result.front() - result.back()) << "\n";

        for (size_t i = 0; i < result.size(); ++i)
        {
            std::cout << result[i] << " ";
        }

        std::cout << "\n\n";
    }
}

int main()
{
    int M, N, val;
    cin >> N >> M;

    cin >> val;

    ImplicitTreapPtr treap = new ImplicitTreap(val);

    for (int i = 2; i <= N; ++i)
    {
        cin >> val;
        ImplicitTreapPtr n = new ImplicitTreap(val);
        ImplicitTreap::merge(treap, treap, n);
    }

    for (int i = 1; i <= M; ++i)
    {
        int T, I, J;
        cin >> T >> I >> J;

        ImplicitTreapPtr left, middle, right;
        ImplicitTreap::split(treap, left, treap, I - 1);
        ImplicitTreap::split(treap, middle, right, J - I + 1);

        if (T == 1)
        {
            ImplicitTreap::merge(treap, middle, left);
            ImplicitTreap::merge(treap, treap, right);
        }
        else
        {
            ImplicitTreap::merge(treap, left, right);
            ImplicitTreap::merge(treap, treap, middle);
        }
    }

    print_treap(treap, true);
    return 0;
}
