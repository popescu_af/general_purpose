//
// See: https://www.hackerrank.com/challenges/common-child
//

#include <iostream>
#include <alg/lcs.h>

using namespace std;

int main()
{
    string s, t;
    getline(cin, s, '\n');
    getline(cin, t, '\n');

    cout << alg::LCSLength(s, t);

    return 0;
}