//
// See: https://www.hackerrank.com/challenges/matrix-rotation-algo
//

#include <ds/rotating_matrix.h>

int main()
{
    uint32_t rows, cols, rotations;
    std::cin >> rows >> cols >> rotations;

    std::vector<uint32_t> elements(rows * cols, 0);

    for (uint32_t i = 0; i < rows; ++i)
    {
        for (uint32_t j = 0; j < cols; ++j)
        {
            std::cin >> elements[i * cols + j];
        }
    }

    ds::RotMatrix<uint32_t> m(rows, cols, elements);
    m.rotate(rotations);

    std::cout << m;
    return 0;
}
