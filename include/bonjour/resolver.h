/**
 *
 * Copyright (c) 2016 - 2018, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#ifndef GP_BONJOUR_RESOLVER_H
#define GP_BONJOUR_RESOLVER_H

#include "common.h"
#include "record.h"

#include <cstdint>

#include <dns_sd.h>

namespace gp_bonjour
{

class Resolver final : private Common
{
public:
    ~Resolver();

    std::future<DNSServiceErrorType> resolve_record(const Record& record);

    std::string get_host() const
    {
        return host_;
    }

    std::uint16_t get_port() const
    {
        return port_;
    }

private:
    static void DNSSD_API resolve_callback(DNSServiceRef, DNSServiceFlags, std::uint32_t, DNSServiceErrorType,
                                           const char*, const char*, std::uint16_t, std::uint16_t,
                                           const unsigned char*, void*);

    std::string host_{""};
    std::uint16_t port_{0};
};

}

#endif // GP_BONJOUR_RESOLVER_H
