/**
 *
 * Copyright (c) 2016 - 2018, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#ifndef GP_BONJOUR_RECORD_H
#define GP_BONJOUR_RECORD_H

#include <string>

namespace gp_bonjour
{

struct Record
{
    Record(const std::string& name = "", const std::string& type = "", const std::string& domain = "")
        : service_name_(name)
        , type_(type)
        , domain_(domain)
    {
    }

    std::string service_name_; /**< 1-63 bytes of UTF-8 text. */
    std::string type_;         /**< _ + 1-15 characters + ._tcp or ._udp, e.g. _example._tcp */
    std::string domain_;

    bool operator==(const Record& other) const
    {
        return (service_name_ == other.service_name_ && type_ == other.type_ && domain_ == other.domain_);
    }

    bool operator!=(const Record& other) const
    {
        return !(*this == other);
    }
};

inline bool operator<(const Record& lhs, const Record& rhs)
{
    return (lhs.service_name_ < rhs.service_name_);
}

}

#endif // GP_BONJOUR_RECORD_H
