/**
 *
 * Copyright (c) 2016 - 2018, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#ifndef GP_BONJOUR_BROWSER_H
#define GP_BONJOUR_BROWSER_H

#include "common.h"
#include "record.h"

#include <condition_variable>
#include <cstdint>
#include <functional>
#include <mutex>
#include <vector>

#include <dns_sd.h>

namespace gp_bonjour
{

class Browser final : private Common
{
public:
    ~Browser();

    using RecordsChangedCallback = std::function<void()>;

    void set_records_changed_callback(RecordsChangedCallback cb)
    {
        cb_ = std::move(cb);
    }

    DNSServiceErrorType browse_services_of_type(const std::string& type);
    void stop_browsing();

    std::string browsing_type() const
    {
        return browsing_type_;
    }

    std::vector<Record> records() const
    {
        std::unique_lock<std::mutex> lock(data_mutex_);
        return records_;
    }

private:
    static void DNSSD_API browse_callback(DNSServiceRef, DNSServiceFlags, std::uint32_t, DNSServiceErrorType,
                                          const char*, const char*, const char*, void*);

    RecordsChangedCallback cb_;

    std::mutex cv_mutex_;
    std::condition_variable end_browsing_;
    std::future<void> browsing_ended_;

    mutable std::mutex data_mutex_;
    std::string browsing_type_{""};
    std::vector<Record> records_;

    std::uint32_t notification_count_{0};
    std::future<void> pending_notification_;
};

}

#endif // GP_BONJOUR_BROWSER_H
