/**
 *
 * Copyright (c) 2016, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#ifndef ROT_MATRIX_H
#define ROT_MATRIX_H

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

namespace ds
{
template <class T> class RotMatrix;
}

template <class T> std::ostream& operator<<(std::ostream& out, const ds::RotMatrix<T>& m);

namespace ds
{
template <class T> class RotMatrix
{
  public:
    RotMatrix(const uint32_t rows, const uint32_t cols, const std::vector<T>& elem)
        : rows_(rows), cols_(cols), index_matrix_(elem.size(), 0), element_rings_(std::min(rows_, cols_) / 2)
    {
        uint32_t r0 = 0, r1 = rows_ - 1, c0 = 0, c1 = cols_ - 1, index_ring = 0;

        for (; index_ring < element_rings_.size(); ++index_ring)
        {
            std::vector<T>& cur_ring = element_rings_[index_ring];

            // Top line
            for (uint32_t i = c0; i <= c1; ++i)
            {
                const uint32_t index = r0 * cols_ + i;
                index_matrix_[index] = RingsIndex(index_ring, cur_ring.size());
                cur_ring.push_back(elem[index]);
            }

            // Right column
            for (uint32_t i = r0 + 1; i <= r1 - 1; ++i)
            {
                const uint32_t index = i * cols_ + c1;
                index_matrix_[index] = RingsIndex(index_ring, cur_ring.size());
                cur_ring.push_back(elem[index]);
            }

            // Bottom line
            for (uint32_t i = c1;; --i)
            {
                const uint32_t index = r1 * cols_ + i;
                index_matrix_[index] = RingsIndex(index_ring, cur_ring.size());
                cur_ring.push_back(elem[index]);

                if (i == c0)
                {
                    break;
                }
            }

            // Left column
            for (uint32_t i = r1 - 1; i >= r0 + 1; --i)
            {
                const uint32_t index = i * cols_ + c0;
                index_matrix_[index] = RingsIndex(index_ring, cur_ring.size());
                cur_ring.push_back(elem[index]);
            }

            ++r0;
            ++c0;
            --r1;
            --c1;
        }
    }

    void rotate(const uint32_t amount)
    {
        for (uint32_t index_ring = 0; index_ring < element_rings_.size(); ++index_ring)
        {
            typename std::vector<T>& ring = element_rings_[index_ring];
            const uint32_t rotate_amount = amount % ring.size();

            typename std::vector<T>::iterator it = ring.begin();
            std::advance(it, rotate_amount);
            std::rotate(ring.begin(), it, ring.end());
        }
    }

  private:
    template <class U> friend std::ostream& ::operator<<(std::ostream& out, const RotMatrix<U>& m);

    struct RingsIndex
    {
        RingsIndex(const uint32_t ri = 0, const uint32_t ei = 0) : ring_index(ri), elem_index(ei) {}

        uint32_t ring_index;
        uint32_t elem_index;
    };

    uint32_t rows_;
    uint32_t cols_;
    std::vector<RingsIndex> index_matrix_;
    std::vector<std::vector<T> > element_rings_;
};
}

template <class T> std::ostream& operator<<(std::ostream& out, const ds::RotMatrix<T>& m)
{
    for (uint32_t i = 0; i < m.rows_; ++i)
    {
        for (uint32_t j = 0; j < m.cols_; ++j)
        {
            const typename ds::RotMatrix<T>::RingsIndex& index = m.index_matrix_[i * m.cols_ + j];
            out << m.element_rings_[index.ring_index][index.elem_index] << " ";
        }
        out << "\n";
    }
    return out;
}

#endif // ROT_MATRIX_H
