/**
 *
 * Copyright (c) 2016, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#ifndef GP_IMPL_TREAP_H
#define GP_IMPL_TREAP_H

#include <iostream>
#include <vector>

namespace ds
{
class ImplicitTreap;
typedef ImplicitTreap* ImplicitTreapPtr;

class ImplicitTreap
{
  public:
    explicit ImplicitTreap(const int val)
        : priority_(rand()), size_(1), value_(val), st_val_(val), lazy_val_(0), l_(NULL), r_(NULL)
    {
    }

    ~ImplicitTreap()
    {
        delete l_;
        delete r_;
    }

    static void split(ImplicitTreapPtr t, ImplicitTreapPtr& l, ImplicitTreapPtr& r, const uint32_t pos,
                      const int add = 0)
    {
        if (!t)
        {
            l = r = NULL;
            return;
        }

        lazy(t);

        const uint32_t curr_pos = add + sz(t->l_);
        if (curr_pos < pos)
        {
            // Element at pos goes to left subtree
            split(t->r_, t->r_, r, pos, curr_pos + 1);
            l = t;
        }
        else
        {
            split(t->l_, l, t->l_, pos, add);
            r = t;
        }
        upd_sz(t);
        operation(t);
    }

    static void merge(ImplicitTreapPtr& t, ImplicitTreapPtr l, ImplicitTreapPtr r)
    {
        lazy(l);
        lazy(r);

        if (!l || !r)
        {
            t = l ? l : r;
        }
        else if (l->priority_ > r->priority_)
        {
            merge(l->r_, l->r_, r);
            t = l;
        }
        else
        {
            merge(r->l_, l, r->l_);
            t = r;
        }
        upd_sz(t);
        operation(t);
    }

    static int range_query(ImplicitTreapPtr t, int l, int r)
    {
        //[l,r]
        ImplicitTreapPtr L, mid, R;
        split(t, L, mid, l - 1);
        split(mid, t, R, r - l); // note: r-l!!

        const int answer = t->st_val_;
        merge(mid, L, t);
        merge(t, mid, R);
        return answer;
    }
    
    static void range_update(ImplicitTreapPtr t, int l, int r, int val)
    {
        //[l,r]
        ImplicitTreapPtr L, mid, R;
        split(t, L, mid, l - 1);
        split(mid, t, R, r - l); // note: r-l!!
        t->lazy_val_ += val;     // lazy_update
        merge(mid, L, t);
        merge(t, mid, R);
    }

    void inorder(std::vector<int>& result)
    {
        if (l_)
        {
            l_->inorder(result);
        }

        result.push_back(value_);

        if (r_)
        {
            r_->inorder(result);
        }
    }

  private:
    // Internal methods
    static uint32_t sz(const ImplicitTreapPtr t) { return t ? t->size_ : 0; }

    static void upd_sz(ImplicitTreapPtr t)
    {
        if (t)
        {
            t->size_ = sz(t->l_) + 1 + sz(t->r_);
        }
    }

    static void lazy(ImplicitTreapPtr t)
    {
        if (!t || !t->lazy_val_)
        {
            return;
        }

        // Operation of lazy
        t->value_ += t->lazy_val_;
        t->st_val_ += t->lazy_val_ * sz(t);

        // Propagate lazy
        if (t->l_)
        {
            t->l_->lazy_val_ += t->lazy_val_;
        }
        if (t->r_)
        {
            t->r_->lazy_val_ += t->lazy_val_;
        }
        t->lazy_val_ = 0;
    }

    static void reset(ImplicitTreapPtr t)
    {
        if (t)
        {
            // No need to reset lazy because when we call this lazy would itself be propagated
            t->st_val_ = t->value_;
        }
    }

    // Combine two ranges of segtree
    static void combine(ImplicitTreapPtr& t, ImplicitTreapPtr l, ImplicitTreapPtr r)
    {
        if (!l || !r)
        {
            t = l ? l : r;
            return;
        }

        t->st_val_ = l->st_val_ + r->st_val_;
    }

    // Operation of segtree
    static void operation(ImplicitTreapPtr t)
    {
        if (!t)
        {
            return;
        }
        reset(t); // Reset the value of current node assuming it now represents a single element of the array
        lazy(t->l_);
        lazy(t->r_); // Propagate lazy before combining t->l_ with t->r_
        combine(t, t->l_, t);
        combine(t, t, t->r_);
    }

    // Attributes
    uint32_t priority_;
    uint32_t size_;
    int value_;          ///< Value
    int st_val_;         ///< Segtree value
    int lazy_val_;       ///< Lazy update value
    ImplicitTreapPtr l_; ///< Left node
    ImplicitTreapPtr r_; ///< Right node
};
}

#endif // GP_IMPL_TREAP_H
