/**
 *
 * Copyright (c) 2016, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#ifndef WRAP_FFMPEG_H
#define WRAP_FFMPEG_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"

#ifdef __cplusplus
extern "C" {
#endif

#include <libavutil/avassert.h>
#include <libavutil/channel_layout.h>
#include <libavutil/opt.h>
#include <libavutil/mathematics.h>
#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>

#ifdef __cplusplus
}
#endif

#pragma GCC diagnostic pop

#include "gp/logger.h"

#define FFMPEG_FAIL(error_code)                                                                                        \
    {                                                                                                                  \
        std::string file(__FILE__);                                                                                    \
        file = file.substr(file.rfind('/') + 1);                                                                       \
        GP_LOG_ERROR("FFMPEG Error %d(file:%s,line:%d)", error_code, file.c_str(), __LINE__);                          \
        return;                                                                                                        \
    }

#define FFMPEG_FAIL_IF_NONZERO(error_code)                                                                             \
    if (error_code)                                                                                                    \
    {                                                                                                                  \
        FFMPEG_FAIL(error_code);                                                                                       \
    }

#define FFMPEG_FAIL_IF_NEGATIVE(error_code)                                                                            \
    if (error_code < 0)                                                                                                \
    {                                                                                                                  \
        FFMPEG_FAIL(error_code);                                                                                       \
    }

#endif // WRAP_FFMPEG_H
