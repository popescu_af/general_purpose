/**
 *
 * Copyright (c) 2016, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#ifndef GP_ERROR_H
#define GP_ERROR_H

#include <cassert>
#include <iostream>
#include <string>

#include "logger.h"

#include <boost/system/error_code.hpp>

namespace gp
{
using ErrorCode = boost::system::error_code;
}

#define GP_ASSERT(condition)                                                                                           \
    assert(condition);                                                                                                 \
    if (!(condition))                                                                                                  \
    {                                                                                                                  \
        std::string file(__FILE__);                                                                                    \
        file = file.substr(file.rfind('/') + 1);                                                                       \
        GP_LOG_ERROR("AssertError(file:%s,line: %d): %s\n", file.c_str(), __LINE__, #condition);                       \
        return;                                                                                                        \
    }

#define GP_FAIL_IF_ERROR(error_code)                                                                                   \
    if (error_code)                                                                                                    \
    {                                                                                                                  \
        std::string file(__FILE__);                                                                                    \
        file = file.substr(file.rfind('/') + 1);                                                                       \
        GP_LOG_ERROR("Error(file:%s,line: %d): %s\n", file.c_str(), __LINE__, error_code.message().c_str());           \
        return;                                                                                                        \
    }

#define GP_SET_ERROR(gp_error_code, gp_type)                                                                           \
    gp_error_code = boost::system::error_code(boost::system::errc::gp_type, boost::system::generic_category())

#endif // GP_ERROR_H
