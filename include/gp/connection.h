/**
 *
 * Copyright (c) 2016, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#ifndef GP_CONNECTION_H
#define GP_CONNECTION_H

#include <cstdint>
#include <deque>
#include <functional>
#include <memory>
#include <mutex>
#include <string>

#include <boost/asio.hpp>
#include <boost/smart_ptr/enable_shared_from_this.hpp>

#include "gp/error.h"

namespace gp
{
namespace inet
{
using Port = std::size_t;

class TcpClient;
struct TcpServerImpl;

class TcpEndpoint : public boost::enable_shared_from_this<TcpEndpoint>
{
  public:
    typedef boost::shared_ptr<TcpEndpoint> pointer;

    TcpEndpoint();
    ~TcpEndpoint();

    //! Start service
    void start();

    //! Stop service
    void stop();

    //! Synchronous send to the client
    void queued_send(const std::string& buffer);

    //! Synchronous receive from the client
    void recv(std::string& result, ErrorCode& error_code, size_t bytes_expected = 0);

  private:
    friend class TcpClient;
    friend struct TcpServerImpl;

    struct TcpEndpointImpl;
    std::unique_ptr<TcpEndpointImpl> impl_;
};

class TcpClient
{
  public:
    TcpClient();
    ~TcpClient();

    //! Connect to a server
    void connect(const std::string& host, const Port port, ErrorCode& error_code);

    //! Disconnect
    void disconnect();

    //! Asynchronous send to the server
    void queued_send(const std::string& buffer);

    //! Synchronous receive from the server
    void recv(std::string& result, ErrorCode& error_code, size_t bytes_expected = 0);

  private:
    TcpEndpoint endpoint_;
};

class AbstractServer
{
  public:
    typedef std::shared_ptr<AbstractServer> pointer;

    virtual ~AbstractServer();

    //! Accepts a new connection.
    /**
     *  NOTE: blocks accepting of subsequent new connections until the current connection finishes serving.
     *  Override if non-blocking behavior is needed.
     */
    virtual void accept(TcpEndpoint::pointer endpoint);

  protected:
    //! Serving method
    virtual void serve() = 0;

    TcpEndpoint::pointer endpoint_;
};

class TcpServer
{
  public:
    explicit TcpServer(AbstractServer::pointer server);
    ~TcpServer();

    //! Starts the TCP asynchronous server
    void start(const Port port);

    //! Stop the server
    void stop();

  private:
    std::unique_ptr<TcpServerImpl> impl_;
};
}
}

#endif // GP_CONNECTION_H
