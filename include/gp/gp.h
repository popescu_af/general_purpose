/**
 *
 * Copyright (c) 2016, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#ifndef GP_GP_H
#define GP_GP_H

// Visibility stuff
#if (defined WIN32 || defined _WIN32 || defined WINCE || defined __CYGWIN__)
#define GP_EXPORT __declspec(dllexport)
#elif(defined __GNUC__ && __GNUC__ >= 4)
#define GP_EXPORT __attribute__((visibility("default")))
#elif(defined __llvm__ || defined __clang__)
#define GP_EXPORT __attribute__((visibility("default")))
#else
#define GP_EXPORT
#endif

// General purpose
#define GP_UNUSED(x) static_cast<void>(x)

#define _STRINGIFY(s) #s
#define STRINGIFY(s) _STRINGIFY(s)

inline bool is_cpu_big_endian()
{
    union
    {
        uint32_t i;
        char b[4];
    }
    word_and_its_bytes = {0x01020304};
    return word_and_its_bytes.b[0] == 1;
}

#endif // GP_GP_H
