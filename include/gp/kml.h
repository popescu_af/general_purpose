/**
 *
 * Copyright (c) 2016, Alexandru Florinel Popescu
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *
 **/

#ifndef GP_KML_H
#define GP_KML_H

#include <fstream>
#include <string>

#include <wrap_3rdparty/wrap_kml.h>

namespace gp
{
namespace kml
{
class File
{
  public:
    File(const std::string& path, const std::ios::openmode mode = std::ios::out)
        : factory_(kmldom::KmlFactory::GetFactory()), kml_(nullptr), document_(nullptr), path_(path), openmode_(mode)
    {
        switch (openmode_)
        {
        case std::ios::out:
            kml_ = factory_->CreateKml();
            document_ = factory_->CreateDocument();
            kml_->set_feature(document_);
            init_style();
            break;

        default:
            break;
        }
    }

    ~File()
    {
        switch (openmode_)
        {
        case std::ios::out:
            kmlbase::File::WriteStringToFile(SerializePretty(kml_), path_);
            break;

        default:
            break;
        }
    }

    void add_placemark(const float latitude, const float longitude, const std::string& name = "")
    {
        using namespace kmldom;

        CoordinatesPtr coords = factory_->CreateCoordinates();
        coords->add_latlng(static_cast<double>(latitude), static_cast<double>(longitude));

        PointPtr pt = factory_->CreatePoint();
        pt->set_coordinates(coords);

        PlacemarkPtr place = factory_->CreatePlacemark();
        place->set_geometry(pt);
        place->set_name(name);

        document_->add_feature(place);
    }

    template <class POINT, float POINT::*latitude_member, float POINT::*longitude_member>
    void add_polygon(const std::vector<POINT>& outer_points, const std::vector<POINT>&, const std::string& name = "")
    {
        using namespace kmldom;

        CoordinatesPtr coords = factory_->CreateCoordinates();

        LinearRingPtr ring = factory_->CreateLinearRing();
        ring->set_coordinates(coords);

        OuterBoundaryIsPtr outer_boundary = factory_->CreateOuterBoundaryIs();
        outer_boundary->AddElement(ring);

        PolygonPtr poly = factory_->CreatePolygon();
        poly->set_outerboundaryis(outer_boundary);

        PlacemarkPtr place = factory_->CreatePlacemark();
        place->set_geometry(poly);
        place->set_name(name);

        document_->add_feature(place);

        for (const auto& rawpt : outer_points)
        {
            coords->add_latlng(static_cast<double>(rawpt.*latitude_member),
                               static_cast<double>(rawpt.*longitude_member));
        }
    }

    template <typename MULTIPT_PTR, class POINT, float POINT::*latitude_member, float POINT::*longitude_member>
    void add_multipoint(const std::vector<POINT>& points, const std::string& name = "")
    {
        using namespace kmldom;

        CoordinatesPtr coords = factory_->CreateCoordinates();

        MULTIPT_PTR multipt = create_multipt_ptr<MULTIPT_PTR>();
        if (multipt == nullptr)
        {
            return;
        }
        multipt->set_coordinates(coords);

        PlacemarkPtr place = factory_->CreatePlacemark();
        place->set_styleurl("#normal");
        place->set_geometry(multipt);
        place->set_name(name);

        document_->add_feature(place);

        for (const auto& rawpt : points)
        {
            coords->add_latlng(static_cast<double>(rawpt.*latitude_member),
                               static_cast<double>(rawpt.*longitude_member));
        }
    }

  private:
    void init_style()
    {
        using namespace kmldom;

        StylePtr style = factory_->CreateStyle();
        style->set_id("normal");

        LineStylePtr line_style = factory_->CreateLineStyle();
        line_style->set_color(kmlbase::Color32(0xff0000ff));
        line_style->set_width(3);

        style->set_linestyle(line_style);
        document_->add_styleselector(style);
    }

    template <typename MULTIPT_PTR> MULTIPT_PTR create_multipt_ptr() { return nullptr; }

    kmldom::KmlFactory* factory_;
    kmldom::KmlPtr kml_;
    kmldom::DocumentPtr document_;
    const std::string path_;
    const std::ios::openmode openmode_;
};

template <> kmldom::LinearRingPtr File::create_multipt_ptr() { return factory_->CreateLinearRing(); }
template <> kmldom::LineStringPtr File::create_multipt_ptr() { return factory_->CreateLineString(); }
}
}

#endif // GP_KML_H
